//
//  PreviewViewController.swift
//  UMOJIswift
//
//  Created by macOS on 10/12/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {
    
    var isFacebookButtonHidden: Bool!
    var isSignUpEmailHidden: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backToViewController(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "backToMenuViewController" {
            if let destinationViewController = segue.destination as? MenuViewController {
                destinationViewController.isFacebookButtonHidden = true
                destinationViewController.isSignUpEmailHidden = true
            }
        }
    }

    @IBAction func OnOKClicked(_ sender: AnyObject) {
        DispatchQueue.main.async
            {
                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "NavController") as? UINavigationController?
                self.present(mapViewControllerObj!!, animated: true, completion: nil)
        }
    }

}
