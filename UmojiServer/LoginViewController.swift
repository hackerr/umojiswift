//
//  LoginViewController.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/22/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import AVFoundation

class LoginViewController: UIViewController {
    @IBOutlet weak var loginLlb: UITextField!
    @IBOutlet weak var passwordLbl: UITextField!

    var textToSpeech : TextToSpeachHelper?
    private var player : AVPlayer = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textToSpeech = TextToSpeachHelper()
        
        
        AdobeUXAuthManager.shared().setAuthenticationParametersWithClientID("2a035a34-bb0e-430f-ad34-83eab1fa314d", clientSecret: "2a0d222a7be24b72b5aed4f4d1fe6657", enableSignUp: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signinClicked(_ sender: AnyObject) {
        HttpHelper.PostRequest(method: "auth/login", params : "email=\(loginLlb!.text!)&password=\(passwordLbl!.text!)")
        {
            (res: GenericResult<LoginModel>) in
                print(res.result.user.first_name)
        }
    }

    @IBAction func signupClicked(_ sender: AnyObject) {
        HttpHelper.PostRequest(method: "auth/signup", params : "email=\(loginLlb!.text!)&password=\(passwordLbl!.text!)&first_name=Artem&last_name=Lemeshev")
        {
            (res: GenericResult<LoginModel>) in
            print(res.result.user.first_name)
        }
        textToSpeech?.TextToSpeach(text: "This is test message to you, my friend!",lang: "en-us")

    }

    @IBAction func facebookClicked(_ sender: AnyObject) {
        let loginManager = LoginManager()
        loginManager.logIn([ .email,.publicProfile, .userFriends ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, _, _):
                FacebookHelper.GetProfile()
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
