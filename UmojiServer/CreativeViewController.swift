//
//  CreativeViewController.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/29/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import UIKit

class CreativeViewController: UIViewController, AdobeUXImageEditorViewControllerDelegate {

    @IBOutlet weak var myImg: UIImageView!
    override func viewDidLoad() {
        myImg.image = UIImage(named: "woman")
        
    }
    
    func photoEditor(_ editor : AdobeUXImageEditorViewController, finishedWith image: UIImage?)
    {
        
    }
    
    func photoEditorCanceled(_ editor : AdobeUXImageEditorViewController)
    {
        
    }
    
    @IBAction func ShowEditor(_ sender: AnyObject) {
        let editor : AdobeUXImageEditorViewController = AdobeUXImageEditorViewController(image: myImg.image! )
        editor.delegate = self
        self.present(editor, animated: false, completion: nil)
    }
}
