import Foundation
import ObjectMapper

public class UserModel : Mappable {
    public var id : Int = 0
    //public var facebook_id : Int = 0
    public var first_name : String = ""
    public var last_name : String = ""
    public var activated : Int = 0
    public var email : String = ""
    public var city : String = ""
    public var user_pic_path : String = ""
    public var is_logged : Int = 0
    public var is_private : Int = 0
    public var push_enabled : Int = 0
    public var created_at : String = ""
    public var updated_at : String = ""
    
    required public init?(_ map: Map) {
        
    }
    
    public init?() {
        
    }
    
    open func mapping(_ map: Map) {
        id <- map["id"]
        //facebook_id <- map["facebook_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        
        activated <- map["activated"]
        email <- map["email"]
        city <- map["city"]
        user_pic_path <- map["user_pic_path"]
        is_logged <- map["is_logged"]
        is_private <- map["is_private"]
        push_enabled <- map["push_enabled"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }

}
