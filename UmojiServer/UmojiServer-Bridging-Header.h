//
//  UmojiServer-Bridging-Header.h
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/29/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

#ifndef UmojiServer_Bridging_Header_h
#define UmojiServer_Bridging_Header_h

#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>
#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>
#endif /* UmojiServer_Bridging_Header_h */
