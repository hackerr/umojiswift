//
//  GenericResult.swift
//  vksdk
//
//  Created by Artem Lemeshev on 6/28/16.
//  Copyright © 2016 Artem Lemeshev. All rights reserved.
//

import Foundation
import ObjectMapper

open class GenericResult<T: Mappable>
{
    open var result : T! = nil
    open var resultArray : Array<T>! = nil
}
