//
//  TextToSpeachHelper.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/29/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

public class TextToSpeachHelper
{

    public func TextToSpeach(text : String, lang : String) -> AVPlayer?
    {
        let url : String = "key=1a1b0947d5b04e88a78669208704df1e&hl=\(lang)&src=\(text)&f=48khz_16bit_stereo".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let urll = URL(string: "http://api.voicerss.org/?\(url)")
        
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: urll!, completionHandler: { (URL, response, error) -> Void in
            
            let player = AVPlayer(url: URL!)
            player.play()
            
        })
        
        downloadTask.resume()
        
        
        let data = NSData(contentsOf: urll!)//(url: urll!)//: urll!)
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as String
        let destinationPath = documentsPath.appending("/test.mp3")
        data?.write(toFile: destinationPath, atomically: false)

        return AVPlayer(url: URL(string: destinationPath)!)
        
    }
}
