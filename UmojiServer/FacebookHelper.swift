//
//  FacebookHelper.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/29/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import Foundation
import FacebookCore
import FacebookLogin
import UIKit

public class FacebookHelper
{
    
    public static var currentUser : UserModel?
    
    public static func GetProfile()
    {
        let request = GraphRequest(graphPath: "me",
                                   parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large),location"],
                                   httpMethod: .GET)
        request.start { httpResponse, result in
            switch result {
            case .success(let response):
                print("Graph Request Succeeded: \(response)")
                let responseDictionary = response.dictionaryValue
                print ("picture \(responseDictionary?["picture"])")
                let profile = UserProfile(userId: (responseDictionary?["id"] as? String)!,
                                          firstName: responseDictionary?["first_name"] as? String,
                                          middleName: responseDictionary?["middle_name"] as? String,
                                          lastName: responseDictionary?["last_name"] as? String,
                                          fullName: responseDictionary?["name"] as? String,
                                          profileURL: (responseDictionary?["link"] as? String).flatMap({ URL(string: $0) }),
                                          email: responseDictionary?["email"] as? String,
                                          city: responseDictionary?["city"] as? String,
                                          picture: responseDictionary?["picture"] as? String,
                                          refreshDate: Date())
                
                
                UserProfile.current = profile
                FacebookHelper.currentUser?.id = -1
                FacebookHelper.currentUser?.first_name = (UserProfile.current?.firstName)!
                FacebookHelper.currentUser?.last_name = (UserProfile.current?.lastName)!
                FacebookHelper.currentUser?.activated = 1
                
                
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
    }
}
