//
//  RegisterModel.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/22/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import Foundation
import ObjectMapper

public class RegisterModel : Mappable
{
    public var user : UserModel
    
    required public init?(_ map: Map) {
        user = UserModel(map)!
    }
    
    open func mapping(_ map: Map) {
        user <- map["data"]
    }


}
