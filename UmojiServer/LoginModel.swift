//
//  LoginModel.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/22/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import Foundation
import ObjectMapper

public class LoginModel : Mappable
{
    public var token : String = ""
    public var user : UserModel
    
    required public init?(_ map: Map) {
        user = UserModel(map)!
    }
    
    open func mapping(_ map: Map) {
        user <- map["user"]
        token <- map["token"]
    }
}
