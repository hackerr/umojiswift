//
//  HttpHelper.swift
//  UmojiServer
//
//  Created by Artem Lemeshev on 9/22/16.
//  Copyright © 2016 Risesoft. All rights reserved.
//

import Foundation
import ObjectMapper

public class HttpHelper
{
    public static var selectedImage : UIImage?
    
    public static func PostRequest<T>(method : String, params : String, res :@escaping (GenericResult<T>) -> ())
    {
        let result : GenericResult<T> = GenericResult<T>()
        var request = URLRequest(url: URL(string: "http://test.gubare1e.bget.ru/api/"+method)!)
        request.httpMethod = "POST"
        request.httpBody = params.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            print (responseString)
            
            if (responseString?.hasPrefix("["))!
            {
                let resData = Mapper<T>().mapArray(responseString! as String)
                result.resultArray = resData
                
                res(result);
            }else{
                let resData = Mapper<T>().map(responseString! as String)
                result.result = resData
                
                res(result);
            }
        }
        task.resume()
    }
    
    
    public static func Request<T>(method : String, res :@escaping (GenericResult<T>) -> ())
    {
        let result : GenericResult<T> = GenericResult<T>()
        let url: URL = URL(string: "http://test.gubare1e.bget.ru/api/"+method)!
        
        let req = NSMutableURLRequest(url: url)
        req.httpMethod = "GET"
        URLSession.shared.dataTask(with: req as URLRequest) { data, response, error in
            if error != nil {
                //Your HTTP request failed.
                print(error?.localizedDescription)
            } else {
                var responseString = NSString(data:data!, encoding:String.Encoding.utf8.rawValue)!
                
                responseString = responseString.replacingOccurrences(of: "{\"response\":",with:  "") as NSString
                
                responseString = responseString.replacingCharacters(in: NSRange.init(location: responseString.length - 1, length: 1), with: "") as NSString
                
                
                
                if(responseString.hasPrefix("["))
                {
                    let resData = Mapper<T>().mapArray(responseString as String)
                    result.resultArray = resData
                    
                    res(result);
                }else{
                    let resData = Mapper<T>().map(responseString as String)
                    result.result = resData
                    
                    res(result);
                }
                
            }
            }.resume()
    }
    
    public static func RequestUrl(url : URL)
    {
        
        
        
        let req = NSMutableURLRequest(url: url)
        req.httpMethod = "GET"
        URLSession.shared.dataTask(with: req as URLRequest) { data, response, error in
            if error != nil {
                //Your HTTP request failed.
                print(error?.localizedDescription)
            } else {
                var responseString = NSString(data:data!, encoding:String.Encoding.utf8.rawValue)!
                
                
                
                responseString = responseString.replacingCharacters(in: NSRange.init(location: responseString.length - 1, length: 1), with: "") as NSString
                
                print(responseString)
            }
            }.resume()
    }
    

}
