//
//  NavigationSegue.swift
//  UMOJIswift
//
//  Created by InFlamesMAC on 25.09.16.
//  Copyright (c) 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class NavigationSegue: UIStoryboardSegue {
    
    override func perform() {
        
        let tabBarController = self.source as! CostumeTabBarController
        let destinationController = self.destination as! UIViewController
        
        for view in tabBarController.placeholderView.subviews as! [UIView] {
            view.removeFromSuperview()
        }
        
        // Add view to placeholder view
        tabBarController.currentViewController = destinationController
        tabBarController.placeholderView.addSubview(destinationController.view)
        
        // Set autoresizing
        /*
         tabBarController.placeholderView.translatesAutoresizingMaskIntoConstraints(false)
        destinationController.view.setTranslatesAutoresizingMaskIntoConstraints(false)
        */
        let horizontalConstraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[v1]-0-|", options: .alignAllTop, metrics: nil, views: ["v1": destinationController.view])
        
        tabBarController.placeholderView.addConstraints(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v1]-0-|", options: .alignAllTop, metrics: nil, views: ["v1": destinationController.view])
        
        tabBarController.placeholderView.addConstraints(verticalConstraint)
        
        tabBarController.placeholderView.layoutIfNeeded()
        destinationController.didMove(toParentViewController: tabBarController)
        
    }

    
   
}
