//
//  WatchTheInstructionBtn.swift
//  UMOJIswift
//
//  Created by macOS on 9/30/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class WatchTheInstructionBtn: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)!
        self.titleLabel!.font = UIFont (name: "Exo2-Light", size: 15)
        let image : UIImageView = UIImageView (frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        image.image = UIImage (named: "infoimage")
        self.addSubview(image)
    }
}
