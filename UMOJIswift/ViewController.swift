//
//  ViewController.swift
//  UMOJIswift
//
//  Created by InFlamesMAC on 22.09.16.
//  Copyright (c) 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var welcomeTextLabel: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var instructionBtn: UIButton!
    
    var myString:NSString = "Welcome to UMOJI"
    var myMutableString = NSMutableAttributedString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.welcomeTextLabel.font = UIFont (name: "Exo2-Light", size: 17)
        self.multiFont()
        self.costumeContinueBtn()
        self.costumeInstructionBtn()
    }
    func multiFont() {
        
        myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSFontAttributeName:UIFont(name: "Exo2-Light", size: 22)!])
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "Exo2-Bold", size: 22)!, range: NSRange(location:10,length:6))
        welcomeLabel.attributedText = myMutableString
        //welcomeTextLabel.font = UIFont (name: "Exo2-Light", size: 20)!
    }
    
    func costumeContinueBtn() {
//        let borderAlpha : CGFloat = 0.7
//        let cornerRadius : CGFloat = 5.0
//        
//        continueBtn.setTitleColor(UIColor.white, for: UIControlState())
//        continueBtn.backgroundColor = UIColor.clear
//        continueBtn.layer.borderWidth = 1.0
//        continueBtn.layer.borderColor = UIColor(white: 1.0, alpha: borderAlpha).cgColor
//        continueBtn.layer.cornerRadius = cornerRadius
//        continueBtn.titleLabel?.font = UIFont (name: "Exo2-Regular", size: 17)!
//        let image : UIImageView = UIImageView (frame: CGRect(x: 190, y: 20, width: 26, height: 18))
//        image.image = UIImage (named: "arrow")
//        self.continueBtn .addSubview(image)
        
        
    }
    
    func costumeInstructionBtn() {
        instructionBtn.titleLabel!.font = UIFont (name: "Exo2-Light", size: 15)
        let image : UIImageView = UIImageView (frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        image.image = UIImage (named: "infoimage")
        self.instructionBtn .addSubview(image)
        
        
    }
}

