//
//  AddPropsViewController.swift
//  UMOJIswift
//
//  Created by Artem Lemeshev on 10/3/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class AddPropsViewController: UIViewController, AdobeUXImageEditorViewControllerDelegate {

    @IBOutlet weak var myImg: UIImageView!
    
    private var editor : AdobeUXImageEditorViewController?
    var selectedImage : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AdobeUXAuthManager.shared().setAuthenticationParametersWithClientID("2a035a34-bb0e-430f-ad34-83eab1fa314d", clientSecret: "2a0d222a7be24b72b5aed4f4d1fe6657", enableSignUp: false)
        if selectedImage != nil
        {
            editor = AdobeUXImageEditorViewController(image: selectedImage )
        }else
        {
            editor = AdobeUXImageEditorViewController(image: selectedImage )
        }
        editor?.delegate = self
        self.present(editor!, animated: false, completion: nil)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func photoEditor(_ editor : AdobeUXImageEditorViewController, finishedWith image: UIImage?)
    {
        UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        self.editor?.dismiss(animated: true, completion: nil)
        HttpHelper.selectedImage = image!
        pushToTabBarController ()
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    }
    
    func photoEditorCanceled(_ editor : AdobeUXImageEditorViewController)
    {
        self.editor?.dismiss(animated: true, completion: nil)
        pushToTabBarController ()
    }
    
    func pushToTabBarController () {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as? MenuViewController
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
