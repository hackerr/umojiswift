//
//  TabBarMakeUmoji.swift
//  UMOJIswift
//
//  Created by macOS on 9/30/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class TabBarMakeUmoji: UIViewController {
    
    @IBOutlet var tabBarButtons: Array<UIButton>!
    @IBOutlet var placeholderView: UIView!
    @IBOutlet weak var selectedMouthTab: UIImageView!
    @IBOutlet weak var selectedPropsTab: UIImageView!
    @IBOutlet weak var selectedTextTab: UIImageView!
    
    var currentViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedMouthTab.isHidden = false
        
        if(tabBarButtons.count > 0) {
            performSegue(withIdentifier: "FirstVC", sender: tabBarButtons[0])
        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let availableIdentifiers = ["FirstVC", "addProps", "addText"]
        
        if availableIdentifiers.contains(segue.identifier!) {
            
            for btn in tabBarButtons {
                btn.isSelected = false
            }
            
            let senderBtn = sender as! UIButton
            senderBtn.isSelected = true
            
            if segue.identifier == "FirstVC" {
                
                selectedMouthTab.isHidden = false
                selectedPropsTab.isHidden = true
                selectedTextTab.isHidden = true
                
            } else if segue.identifier == "addProps" {
                
                selectedMouthTab.isHidden = true
                selectedPropsTab.isHidden = false
                selectedTextTab.isHidden = true
                
            } else if segue.identifier == "addText" {
                
                selectedMouthTab.isHidden = true
                selectedPropsTab.isHidden = true
                selectedTextTab.isHidden = false
                
            }

        }
    }
}
