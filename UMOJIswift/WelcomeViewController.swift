//
//  ViewController.swift
//  UMOJIswift
//
//  Created by InFlamesMAC on 22.09.16.
//  Copyright (c) 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit


class WelcomeViewController: UIViewController {

    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var welcomeTextLabel: UILabel!
    @IBOutlet weak var continueBtn: ContinueBtn!
    @IBOutlet weak var instructionBtn: WatchTheInstructionBtn!
    
    var myString:NSString = "Welcome to UMOJI"
    var myMutableString = NSMutableAttributedString()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.multiFont()
    }
    
    func multiFont() {
        
        //self.welcomeTextLabel.font = UIFont (name: "Exo2-Light", size: 17)
        
        myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSFontAttributeName:UIFont(name: "Exo2-Light", size: 22)!])
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "Exo2-Bold", size: 22)!, range: NSRange(location:10,length:6))
        welcomeLabel.attributedText = myMutableString
    }
}

