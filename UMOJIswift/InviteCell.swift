//
//  InviteCell.swift
//  UMOJIswift
//
//  Created by macOS on 10/4/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class InviteCell: UITableViewCell {

    @IBOutlet weak var bcg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        bcg.backgroundColor = UIColor(red:0.22, green:0.24, blue:0.28, alpha:1.0)
        bcg.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
