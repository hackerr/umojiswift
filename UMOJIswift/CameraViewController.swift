    //
//  CameraViewController.swift
//  UMOJIswift
//
//  Created by macOS on 10/3/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit
import AVFoundation
    
class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AdobeUXImageEditorViewControllerDelegate{
 
    @IBOutlet weak var bckBtn: UIButton!
    
    @IBOutlet weak var cameraPreview: UIView!
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var bgImage: UIImageView!
    private var editor : AdobeUXImageEditorViewController?

    var frontCamera : Bool = true
    var preview: AVCaptureVideoPreviewLayer?
    var captureSession = AVCaptureSession()
    var stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        bgImage.contentMode = .scaleAspectFit
        AdobeUXAuthManager.shared().setAuthenticationParametersWithClientID("2a035a34-bb0e-430f-ad34-83eab1fa314d", clientSecret: "2a0d222a7be24b72b5aed4f4d1fe6657", enableSignUp: false)
        self.initCamera()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onCameraClicked(_ sender: AnyObject) {
        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                //.editor = AdobeUXImageEditorViewController(image: UIImage(data: imageData!)! )
                //self.editor?.delegate = self
                //self.present(self.editor!, animated: false, completion: nil)
                HttpHelper.selectedImage = UIImage(data: imageData!)!
                self.pushToTabBarController()
            }
        }
    }
    
    func initCamera()
    {
        // Do any additional setup after loading the view, typically from a nib.
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        let devices = AVCaptureDevice.devices()
        
        // Loop through all the capture devices on this phone
        for device in devices! {
            // Make sure this particular device supports video
            if ((device as AnyObject).hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if frontCamera == false
                {
                    if((device as AnyObject).position == AVCaptureDevicePosition.back) {
                        captureDevice = device as? AVCaptureDevice
                        if captureDevice != nil {
                            print("Capture device found")
                            beginSession()
                        }
                    }
                }else
                {
                    if((device as AnyObject).position == AVCaptureDevicePosition.front) {
                        captureDevice = device as? AVCaptureDevice
                        if captureDevice != nil {
                            print("Capture device found")
                            beginSession()
                        }
                    }
                }
            }
        }
    }
    
    func beginSession() {
        configureDevice()
        var err : NSError? = nil
        
        var deviceInput: AVCaptureDeviceInput!
        do {
            deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            
        } catch let error as NSError {
            err = error
            deviceInput = nil
        };
        
        
        captureSession.addInput(deviceInput)
        
        if err != nil {
            print("error: \(err?.localizedDescription)")
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
        
        self.cameraPreview.layer.addSublayer(previewLayer!)
        previewLayer?.frame = self.view.layer.frame
        captureSession.startRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        bgImage.image = chosenImage
        
        dismiss(animated:true, completion: nil) //5
        
        //editor = AdobeUXImageEditorViewController(image: chosenImage )
        //editor?.delegate = self
        //self.present(editor!, animated: false, completion: nil)
        HttpHelper.selectedImage = chosenImage
        pushToTabBarController()
    }
    
    func pushToTabBarController () {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as? TabBarMakeUmoji
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onGalleryClicked(_ sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func photoEditor(_ editor : AdobeUXImageEditorViewController, finishedWith image: UIImage?)
    {
        bgImage.image! = image!
        UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        self.editor?.dismiss(animated: true, completion: nil)
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    }
    
    func photoEditorCanceled(_ editor : AdobeUXImageEditorViewController)
    {
        self.editor?.dismiss(animated: true, completion: nil)
    }
    
    func updateDeviceSettings(focusValue : Float, isoValue : Float) {
        let error: NSErrorPointer = nil
        
        if let device = captureDevice {
            do {
                try captureDevice!.lockForConfiguration()
                
            } catch let error1 as NSError {
                //error?.memory = error1
            }
            
            if frontCamera == false
            {
                device.setFocusModeLockedWithLensPosition(focusValue, completionHandler: { (time) -> Void in
                //
                })
            }
            
            // Adjust the iso to clamp between minIso and maxIso based on the active format
            let minISO = device.activeFormat.minISO
            let maxISO = device.activeFormat.maxISO
            let clampedISO = isoValue * (maxISO - minISO) + minISO
            
            device.setExposureModeCustomWithDuration(AVCaptureExposureDurationCurrent, iso: clampedISO, completionHandler: { (time) -> Void in
                //
            })
            
            device.unlockForConfiguration()
            
        }
    }
    
    func configureDevice() {
        let error: NSErrorPointer = nil
        if let device = captureDevice {
            //device.lockForConfiguration(nil)
            
            do {
                try captureDevice!.lockForConfiguration()
                
            } catch let error1 as NSError {
                //error?.memory = error1
            }
            if frontCamera == false
            {
                device.focusMode = .locked
            }
            device.unlockForConfiguration()
        }
        
    }
    
    @IBAction func onFlashClicked(_ sender: AnyObject) {
        
        if frontCamera == true
        {
            changeCamera()
        }
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureTorchMode.on) {
                    device?.torchMode = AVCaptureTorchMode.off
                } else {
                    try device?.setTorchModeOnWithLevel(1.0)
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
        
    }
    
    @IBAction func onChangeCameraClicked(_ sender: AnyObject) {
        changeCamera()
    }
    
    func changeCamera()
    {
        frontCamera = !frontCamera
        reloadCamera()
    }
    
    func reloadCamera() {
        
        captureSession.stopRunning()
        previewLayer?.removeFromSuperlayer()
        
        // camera loading code
        captureSession = AVCaptureSession()
        
        
            let devices = AVCaptureDevice.devices()
            // Loop through all the capture devices on this phone
            for device in devices! {
                // Make sure this particular device supports video
                if ((device as AnyObject).hasMediaType(AVMediaTypeVideo)) {
                    // Finally check the position and confirm we've got the back camera
                    if frontCamera == false
                    {
                        if((device as AnyObject).position == AVCaptureDevicePosition.back) {
                            captureDevice = device as? AVCaptureDevice
                            if captureDevice != nil {
                                //print("Capture device found")
                                beginSession()
                            }
                        }
                    }else
                    {
                        if((device as AnyObject).position == AVCaptureDevicePosition.front) {
                            captureDevice = device as? AVCaptureDevice
                            if captureDevice != nil {
                                //print("Capture device found")
                                beginSession()
                            }
                        }
                    }
                }
    }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //self.navigationController.popViewControllerAnimated(true);

}
