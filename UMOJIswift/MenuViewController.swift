//
//  MenuViewController.swift
//  UMOJIswift
//
//  Created by InFlamesMAC on 23.09.16.
//  Copyright (c) 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit
import FacebookLogin

class MenuViewController: UIViewController {
    
    
    @IBOutlet weak var loginFacebookBtn:  LoginFacebookBtn!
    @IBOutlet weak var signUpEmail: UIButton!
    
    weak var thankYouViewController : ThankYouViewController!
    
    
    var isFacebookButtonHidden: Bool = false
    var isSignUpEmailHidden: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        costumesignUpEmail()
        
        if FacebookHelper.currentUser != nil && FacebookHelper.currentUser?.id != 0
        {
            loginFacebookBtn.isHidden = true
            signUpEmail.isHidden = true
        }
        
    }
    
    func costumesignUpEmail() {
        signUpEmail.titleLabel!.font = UIFont (name: "Exo2-Light", size: 15)
    }
    
    func pushToFriendsTabBar () {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "FriendsTabBar") as? FriendsTabBarViewController
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
    }

    @IBAction func popToFriendsTabBar(_ sender: AnyObject) {
        pushToFriendsTabBar ()
    }
        
    @IBAction func onFacebookLoginClicked(_ sender: AnyObject) {
        let loginManager = LoginManager()
        loginManager.logIn([ .email,.publicProfile, .userFriends ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, _, _):
                DispatchQueue.main.async {
                    self.loginFacebookBtn.isHidden = true
                    self.signUpEmail.isHidden = true
                }
                
                FacebookHelper.GetProfile()
            }
        }
    }
}

