//
//  ThankYouViewController.swift
//  UMOJIswift
//
//  Created by macOS on 10/4/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var welcomeTextLabel: UILabel!
    @IBOutlet weak var continueBtn: ContinueBtn!
    @IBOutlet weak var instructionBtn: WatchTheInstructionBtn!
    
    var isFacebookButtonHidden: Bool!
    var isSignUpEmailHidden: Bool!

    @IBAction func OnContinue(_ sender: AnyObject) {
        pushToSignInViewController()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    
    }
    
    func pushToSignInViewController () {
        DispatchQueue.main.async
            {
                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as? SignInViewController
                self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "userSignInUmoji" {
            if let destinationViewController = segue.destination as? MenuViewController {
                destinationViewController.isFacebookButtonHidden = true
                destinationViewController.isSignUpEmailHidden = true
            }
        }
    }
    
}
