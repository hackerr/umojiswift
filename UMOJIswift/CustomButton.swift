//
//  CustomButton.swift
//  UMOJIswift
//
//  Created by macOS on 9/27/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
//    func buttonWithParams(button : UIButton,
//                          textBtn : NSString,
//                          font : UIFont,
//                          backgroundImage : NSString) {
//        button.setImage(UIImage(named:backgroundImage as String), for: UIControlState.normal)
//        button.imageEdgeInsets = UIEdgeInsets(top: 6,left: 100,bottom: 6,right: 14)
//        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: -30,bottom: 0,right: 34)
//        button.setTitle(textBtn as String, for: UIControlState.normal)
//        button.layer.borderWidth = 1.0
//        button.layer.borderColor = UIColor.white.cgColor
//    }
    
    func continueBtn(btn : UIButton) {
        let borderAlpha : CGFloat = 0.7
        let cornerRadius : CGFloat = 5.0
        
        btn.setTitleColor(UIColor.white, for: UIControlState())
        btn.backgroundColor = UIColor.clear
        btn.layer.borderWidth = 1.0
        btn.layer.borderColor = UIColor(white: 1.0, alpha: borderAlpha).cgColor
        btn.layer.cornerRadius = cornerRadius
        btn.titleLabel?.font = UIFont (name: "Exo2-Regular", size: 17)!
        let image : UIImageView = UIImageView (frame: CGRect(x: 190, y: 20, width: 26, height: 18))
        image.image = UIImage (named: "arrow")
        btn .addSubview(image)

    }
    func buttonWithParams (button : UIButton,
                           buttonBcg : UIImage,
                           fontName : NSString,
                           fontSize : CGFloat,
                           iconBtn : NSString,
                           withWidth :CGFloat,
                           andHight : CGFloat) {
        
//        button.setImage(UIImage(named:buttonBcg as String), for: UIControlState.normal)
        button.setBackgroundImage(buttonBcg, for: UIControlState.normal)
        button.titleLabel?.font = UIFont (name: fontName as String, size: fontSize)!
        button.imageEdgeInsets = UIEdgeInsets(top: 6,left: 100,bottom: 6,right: 14)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: -30,bottom: 0,right: 34)
        
        
        let image : UIImageView = UIImageView (frame: CGRect(x: 190, y: 20, width: withWidth, height: andHight))
        image.image = UIImage (named: iconBtn as String)
        button .addSubview(image)

        
        
    
    }
}
