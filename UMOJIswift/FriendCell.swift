//
//  FriendCell.swift
//  UMOJIswift
//
//  Created by macOS on 10/4/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {

    @IBOutlet weak var bck: UIView!
    @IBOutlet weak var checkBox: UIImageView!
    @IBOutlet weak var userLocation: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var addUserBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //bck.backgroundColor = UIColor(red:0.22, green:0.24, blue:0.28, alpha:1.0)
        bck.backgroundColor = UIColor.clear
    }
    
    func cornerRadiusImage () {
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2;
        avatarImageView.clipsToBounds = true;
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
