//
//  RecordPhotoTimingsViewController.swift
//  UMOJIswift
//
//  Created by macOS on 9/29/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class RecordPhotoTimingsViewController: UIViewController {

    @IBOutlet weak var listenPartBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        costumeLoginFacebook()
        
}
    
    func costumeLoginFacebook() {
        let borderAlpha : CGFloat = 0.7
        let cornerRadius : CGFloat = 5.0
        
        //listenPartBtn.setTitleColor(UIColor.white, for: UIControlState())
        listenPartBtn.backgroundColor = UIColor.clear
        listenPartBtn.layer.cornerRadius = cornerRadius
        listenPartBtn.titleLabel?.font = UIFont (name: "Exo2-Regular", size: 18)!
        let image : UIImageView = UIImageView (frame: CGRect(x: 15, y: 10, width: 24, height: 24))
        image.image = UIImage (named: "sound")
        self.listenPartBtn .addSubview(image)
        
    }
}
