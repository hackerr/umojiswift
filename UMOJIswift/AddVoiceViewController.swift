//
//  AddVoiceViewController.swift
//  UMOJIswift
//
//  Created by macOS on 10/3/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class AddVoiceViewController: UIViewController {
    
    @IBOutlet weak var bckBtn: UIButton!
    @IBOutlet weak var photoImg: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        photoImg.image = HttpHelper.selectedImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToRootViewController(_ sender: AnyObject) {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "NavController") as? UINavigationController?
        self.present(mapViewControllerObj!!, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
