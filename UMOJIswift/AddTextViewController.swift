//
//  AddTextViewController.swift
//  UMOJIswift
//
//  Created by Artem Lemeshev on 10/11/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class AddTextViewController: UIViewController, AdobeUXImageEditorViewControllerDelegate {

    @IBOutlet weak var sdkView: UIView!
    private var editor : AdobeUXImageEditorViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AdobeUXAuthManager.shared().setAuthenticationParametersWithClientID("2a035a34-bb0e-430f-ad34-83eab1fa314d", clientSecret: "2a0d222a7be24b72b5aed4f4d1fe6657", enableSignUp: false)
        AdobeImageEditorCustomization.setToolOrder([kAVYText])
        
        
        editor = AdobeUXImageEditorViewController(image: HttpHelper.selectedImage! )
        editor?.delegate = self
        editor?.view.frame = sdkView.bounds
        editor?.view.bounds = sdkView.bounds
        self.sdkView.addSubview((editor?.view)!)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func photoEditor(_ editor : AdobeUXImageEditorViewController, finishedWith image: UIImage?)
    {
        UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        HttpHelper.selectedImage = image!
        pushToTabBarController()
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    }
    
    func photoEditorCanceled(_ editor : AdobeUXImageEditorViewController)
    {
        pushToTabBarController()
    }
    
    func pushToTabBarController () {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "NavController") as? UINavigationController?
            self.present(mapViewControllerObj!!, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
