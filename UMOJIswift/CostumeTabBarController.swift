//
//  CostumeTabBarController.swift
//  UMOJIswift
//
//  Created by InFlamesMAC on 25.09.16.
//  Copyright (c) 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class CostumeTabBarController: UIViewController {
    @IBOutlet weak var selectedHomeBtnImg: UIImageView!
    @IBOutlet weak var selectedProfileBtnImg: UIImageView!
    @IBOutlet weak var selectedGaleryBtnImg: UIImageView!
    @IBOutlet weak var selectedFriendBtnImg: UIButton!
    
    @IBOutlet var tabSelectImage: Array<UIImageView>!

    var currentViewController: UIViewController?
    @IBOutlet var placeholderView: UIView!
    @IBOutlet var tabBarButtons: Array<UIButton>!
    
    @IBOutlet weak var bcgTabBar: UIView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //UIColor(red:0.20, green:0.24, blue:0.27, alpha:1.0)
        

        if(tabBarButtons.count > 0) {
            performSegue(withIdentifier: "firstVC", sender: tabBarButtons[0])
        }

    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        
//        let availableIdentifiers = ["firstVC", "secondVC,fourVC,thirdVC"]
//        
//        if availableIdentifiers.contains(segue.identifier!) {
//            
//            for btn in tabBarButtons {
//                btn.isSelected = false
//                btn.alpha = 0.1;
//            }
//            
//            let senderBtn = sender as! UIButton
//            senderBtn.isSelected = true
//            if (senderBtn.isSelected == true) {
//                senderBtn.alpha = 1;
//            }
//        }
//    }

}
