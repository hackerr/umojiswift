//
//  FriendsTabBarViewController.swift
//  UMOJIswift
//
//  Created by macOS on 10/24/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class FriendsTabBarViewController: UIViewController {
    
    @IBOutlet var tabBarButtons: Array<UIButton>!
    @IBOutlet var placeholderView: UIView!
    @IBOutlet weak var selectedChatTab: UIImageView!
    @IBOutlet weak var selectedFriendsTab: UIImageView!
    @IBOutlet weak var selectedProfileTab: UIImageView!
    
    var currentViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        //selectedMouthTab.isHidden = false
        
        if(tabBarButtons.count > 0) {
            performSegue(withIdentifier: "friend", sender: tabBarButtons[0])
        }
    }
    override var shouldAutorotate : Bool {
        return true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let availableIdentifiers = ["profile", "friends", "chat"]
        
        if availableIdentifiers.contains(segue.identifier!) {
            
            for btn in tabBarButtons {
                btn.isSelected = false
            }
            
            let senderBtn = sender as! UIButton
            senderBtn.isSelected = true
            
            if segue.identifier == "profile" {
                
//                selectedMouthTab.isHidden = false
//                selectedPropsTab.isHidden = true
//                selectedTextTab.isHidden = true
                
            } else if segue.identifier == "friends" {
                
//                selectedMouthTab.isHidden = true
//                selectedPropsTab.isHidden = false
//                selectedTextTab.isHidden = true
                
            } else if segue.identifier == "chat" {
                
//                selectedMouthTab.isHidden = true
//                selectedPropsTab.isHidden = true
//                selectedTextTab.isHidden = false
                
            }
            
        }
    }

}
