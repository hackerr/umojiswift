//
//  LoginFacebookBtn.swift
//  UMOJIswift
//
//  Created by macOS on 10/4/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class LoginFacebookBtn: UIButton {
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.setTitleColor(UIColor.white, for: UIControlState())
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = 5.0
        self.titleLabel?.font = UIFont (name: "Exo2-Regular", size: 18)!
        let image : UIImageView = UIImageView (frame: CGRect(x: 45, y: 10, width: 31, height: 31))
        image.image = UIImage (named: "facebookLogo")
        self.addSubview(image)
        
    }
//    override public func layoutSubviews() {
//        super.layoutSubviews()
//        
//    }
}

