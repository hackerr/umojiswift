//
//  CustomButton.swift
//  UMOJIswift
//
//  Created by macOS on 9/27/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class ContinueBtn: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.setTitleColor(UIColor.white, for: UIControlState())
        self.backgroundColor = UIColor.clear
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(white: 1.0, alpha: 0.7).cgColor
        self.layer.cornerRadius = 5.0
        self.titleLabel?.font = UIFont (name: "Exo2-Regular", size: 17)!
        let image : UIImageView = UIImageView (frame: CGRect(x: 190, y: 20, width: 26, height: 18))
        image.image = UIImage (named: "arrow")
        self .addSubview(image)
        
    }
}

