//
//  FriendsViewController.swift
//  UMOJIswift
//
//  Created by macOS on 10/4/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var friendsTableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var bcg: UIImageView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var addContactBtn: UIButton!
    @IBOutlet weak var checkBoxImage: UIImageView!
    
    var roundView : UIView!
    
    let firstCellFrame : CGFloat = 70
    let friendCellFrame : CGFloat = 95
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        friendsTableView.backgroundColor = UIColor.clear
        searchTextField.isHidden = true
        
        addRoundView()
        
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changePosition(_ sender: AnyObject) {
        self.checkPosition()

    }
    
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func addTapGestureReconizer() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FriendsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.friendsTableView.addGestureRecognizer(tap)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        self.checkPosition()
        return true
    }
    
    func addRoundView() {
        
        roundView = UIView(frame: CGRect(x: 324,
                                             y: 30,
                                             width: 30,
                                             height: 42))
        roundView.layer.cornerRadius = roundView.frame.width / 2.0
        roundView.backgroundColor = UIColor.white
        self.bcg.addSubview(roundView)
        roundView.isHidden = true
        
    }
    
    func checkPosition() {
        
        if searchTextField.isHidden == true {
            tittleLabel.isHidden = true
            self.openTextSearchTextField()
            self.reloadTableView()
        } else if searchTextField.isHidden == false {
            self.closeTextSearchTextField()
            self.reloadTableView()
        }
        
    }
    
    func openTextSearchTextField(){
        
        self.roundView.isHidden = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchButton.frame = CGRect(x: 81,
                                             y: self.searchButton.frame.origin.y,
                                             width: self.searchButton.frame.width,
                                             height: self.searchButton.frame.height)
            self.roundView.frame = (CGRect(x: 63,
                                      y: 30,
                                      width: 296,
                                      height: 42))
            
            })
        
        self.tittleLabel.isHidden = false
        self.searchTextField.becomeFirstResponder()
        self.searchButton.isHidden = true
        self.searchTextField.isHidden = false
        let imageView = UIImageView(frame: CGRect(x: 15, y: 10, width: 25, height: 25))
        imageView.image = UIImage(named: "searchActive")
        self.roundView.addSubview(imageView)
        self.customSearchTextField()
    }
    
    func closeTextSearchTextField(){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchButton.frame = CGRect(x: 324,
                                             y: self.searchButton.frame.origin.y,
                                             width: self.searchButton.frame.width,
                                             height: self.searchButton.frame.height)
            self.roundView.frame = (CGRect(x: 324,
                                      y: 30,
                                      width: 30,
                                      height: 42))
            })
        self.tittleLabel.isHidden = false
        self.searchTextField.isHidden = true
        self.roundView.isHidden = true
        self.searchButton.isHidden = false
    }
    
    func customSearchTextField () {
        
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:50, height:0))
        searchTextField.leftViewMode = UITextFieldViewMode.always
        searchTextField.leftView = spacerView
        
        let font = UIFont(name: "Exo2-Light", size: 15)!
        let attributes = [
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSFontAttributeName : font]
        
//        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search" as String,
//                                                             attributes:attributes)
    }
    
    func reloadTableView() {
        
        DispatchQueue.main.async{
            self.friendsTableView.reloadData()
        }
    }
    
    //MARK - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return firstCellFrame
        }
        return friendCellFrame
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let firstCell = friendsTableView.dequeueReusableCell(withIdentifier: "invite", for: indexPath) as! InviteCell
            return firstCell
        }
            
        else {
           let friendCell = friendsTableView.dequeueReusableCell(withIdentifier: "friendCell", for: indexPath) as! FriendCell
            if searchTextField.isHidden == true {
                friendCell.addUserBtn.isHidden = true
                friendCell.checkBox.isHidden = false
            } else if searchTextField.isHidden == false {
                friendCell.addUserBtn.isHidden = false
                friendCell.checkBox.isHidden = true
            }
            return friendCell
        }
    }
}
