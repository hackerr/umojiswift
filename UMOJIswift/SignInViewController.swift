//
//  SignInViewController.swift
//  UMOJIswift
//
//  Created by InFlamesMAC on 23.09.16.
//  Copyright (c) 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit
import FacebookLogin

class SignInViewController: UIViewController{
    
    
    @IBOutlet weak var bcgView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var bckBtn: UIButton?
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var signInCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var loginWithFacebookBtn: LoginFacebookBtn!
    @IBOutlet weak var signInBtn: ContinueBtn!
    
    var segueIdentifire : NSString = "signInOrSignUp"
    
    var segue : UIStoryboardSegue!
    
    
    override func viewDidLoad() {
        
        self.addTapGestureReconizer()
        textFieldOnView()
        self.customeSegment()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bcg")!)
        
        signInBtn.setTitle("SignIn", for: .normal)
        self.signInCOnstraint.constant = -66
        InitFirstTime()
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func addTapGestureReconizer() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func customeSegment() {
        
        segment.frame = CGRect(x: segment.frame.origin.x, y: segment.frame.origin.y, width: segment.frame.size.width, height: 80);
        segment.backgroundColor = UIColor.clear
        
        let attr = NSDictionary(object: UIFont(name: "Exo2-Light", size: 19)!, forKey: NSFontAttributeName as NSCopying)
        
        segment.setTitleTextAttributes(attr as [NSObject : AnyObject] , for: .normal)
        segment.removeBorders()
        
        
        let backgroundImage = UIImage(named: "segmented_separator_bg")
        let dividerImage = UIImage(named: "segmented_separator_bg")
        let backgroundImageSelected = UIImage(named: "segmented_selected_bg")
        
        
        segment.setBackgroundImage(backgroundImage, for: UIControlState(), barMetrics: .default)
        segment.setBackgroundImage(backgroundImageSelected, for: .highlighted, barMetrics: .default)
        segment.setBackgroundImage(backgroundImageSelected, for: .selected, barMetrics: .default)
        
        segment.setDividerImage(dividerImage, forLeftSegmentState: UIControlState(), rightSegmentState: .selected, barMetrics: .default)
        segment.setDividerImage(dividerImage, forLeftSegmentState: .selected, rightSegmentState: UIControlState(), barMetrics: .default)
        segment.setDividerImage(dividerImage, forLeftSegmentState: UIControlState(), rightSegmentState: UIControlState(), barMetrics: .default)

    }
    
    @IBAction func onBackClick(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func segmentedControlAction(sender: AnyObject) {
        
        if segment.selectedSegmentIndex == 1 {
            
            self.signInBtn.setTitle("SignUP", for: .normal)
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: {
                self.signInCOnstraint.constant = 36
                self.repeatPassword.isHidden = false;
                self.view.layoutIfNeeded()
            })
            
            
        }
        else if segment.selectedSegmentIndex == 0 {

            self.signInBtn.setTitle("SignIN", for: .normal)
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: {
                self.signInCOnstraint.constant = -66
                self.view.layoutIfNeeded()
            })
            self.repeatPassword.isHidden = true;
            
            
        }
    }
    @IBAction func signInOrSignUpAction(_ sender: AnyObject) {
        
    }
    
    func InitFirstTime()
    {
        self.signInBtn.setTitle("SignIN", for: .normal)
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            self.signInCOnstraint.constant = -66
            self.view.layoutIfNeeded()
        })
        self.repeatPassword.isHidden = true;

    }
    


    func costumeForgotBtn() {
        forgotBtn.titleLabel!.font = UIFont (name: "Exo2-Light", size: 15)
    }
    
    func textFieldOnView () {
       customeTextFieldWithParams(textField: loginTextField, imageName: "avatarWithLine", placeholderText: "E-mail")
        customeTextFieldWithParams(textField: passwordTextField, imageName: "passwordWithLine", placeholderText: "Enter paasword")
        customeTextFieldWithParams(textField: repeatPassword, imageName: "passwordWithLine", placeholderText: "Repeat paasword")
    }
    
    func customeTextFieldWithParams (textField : UITextField, imageName : NSString, placeholderText : NSString) {
        
        let image : UIImageView = UIImageView (frame: CGRect(x: textField.frame.origin.x, y: textField.frame.origin.y, width: textField.frame.width, height: textField.frame.height))
        image.image = UIImage (named: imageName as String)
        
        textField.background = image.image
        
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:50, height:0))
        textField.leftViewMode = UITextFieldViewMode.always
        textField.leftView = spacerView
        
        let font = UIFont(name: "Exo2-Light", size: 15)!
        let attributes = [
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSFontAttributeName : font]
        
        textField.attributedPlaceholder = NSAttributedString(string: placeholderText as String,
                                                                  attributes:attributes)
    }
    
    func signUP () {
        if  passwordTextField.text == repeatPassword.text
        {
        HttpHelper.PostRequest(method: "auth/signup", params : "email=\(loginTextField!.text!)&password=\(passwordTextField!.text!)&first_name=Artem&last_name=Lemeshev") {
            (res: GenericResult<LoginModel>) in
            
            if res != nil && res.result != nil && res.result.user.id != 0 {
                print("prepare for segue")
                FacebookHelper.currentUser = res.result.user
                self.pushToThankYouViewController()
                return
            }
            else {
                self.ShowError(text: "Пользователь уже существует")
                return
            }
        }
        }else
        {
            ShowError(text: "Пароли не совпадают")
        }

    }
    
    func signIn () {
        HttpHelper.PostRequest(method: "auth/login", params : "email=\(loginTextField!.text!)&password=\(passwordTextField!.text!)"){
            (res: GenericResult<LoginModel>) in
            if res != nil && res.result != nil && res.result.user.id != 0 {
                FacebookHelper.currentUser = res.result.user
                if res.result.user.activated == 1
                {
                    self.pushToTMenuViewController()
                }else
                {
                    self.ShowError(text: "Активируйте аккаунт")
                }
                return
            }
            else {
                self.ShowError(text: "Не верный пароль или же пользователя не существует")
                return
            }
            
        }
    }
    
    func pushToThankYouViewController () {
        DispatchQueue.main.async
        {
            let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController") as? ThankYouViewController
            self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        }
    }
    
    func pushToTMenuViewController () {
        DispatchQueue.main.async
            {
                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "NavController") as? UINavigationController?
                self.present(mapViewControllerObj!!, animated: true, completion: nil)
        }
    }

    
    func ShowError(text : String)
    {
        DispatchQueue.main.async{
            let alertController = UIAlertController(title: "Umoji", message:
                text, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }

    // MARK : - TextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            //self.animateTextField(textField: loginTextField, up:true)
        }
        
        if textField == passwordTextField {
            self.animateTextField(textField: passwordTextField, up:true)
        }
        if textField == repeatPassword {
            self.animateTextField(textField: repeatPassword, up:true)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            //self.animateTextField(textField: loginTextField, up:false)
        }

        if textField == passwordTextField {
            self.animateTextField(textField: passwordTextField, up:false)
        }
    
        if textField == repeatPassword {
            self.animateTextField(textField: repeatPassword, up:false)
        }

        return true
    }

    @IBAction func onSigninClicked(_ sender: AnyObject) {
        if segment.selectedSegmentIndex == 0 {
            self.signIn()
            print("signUp")
        }
        if segment.selectedSegmentIndex == 1 {
            self.signUP()
            print("signIn")
        }
    }
    
    @IBAction func onFacebookLoginClicked(_ sender: AnyObject) {
        let loginManager = LoginManager()
        loginManager.logIn([ .email,.publicProfile, .userFriends ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, _, _):
                FacebookHelper.currentUser = UserModel()
                FacebookHelper.currentUser?.id = -1
                FacebookHelper.GetProfile()
                
                self.pushToTMenuViewController()
            }
        }
    }
    
    //MARK : - textField focuse
    
    func animateTextField(textField: UITextField, up: Bool) {
        let movementDistance:CGFloat = -130
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up {
            movement = movementDistance
        }
        else {
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }

}


    
    

