//
//  CreateSlideshowViewController.swift
//  UMOJIswift
//
//  Created by macOS on 9/28/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit

class CreateSlideshowViewController: UIViewController {

    @IBOutlet weak var addPhoto: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.custumeAddPhotoBtn()
        

    }
        func custumeAddPhotoBtn () {
            
            let borderAlpha : CGFloat = 0.7
            let cornerRadius : CGFloat = 5.0
            
            let color : UIColor = UIColor(red:0.31, green:0.61, blue:0.96, alpha:1.0)
            
            addPhoto.setTitleColor(color, for: UIControlState())
            addPhoto.backgroundColor = UIColor.clear
            addPhoto.layer.borderWidth = 1.0
            addPhoto.layer.borderColor = color.cgColor
            addPhoto.layer.cornerRadius = cornerRadius
            addPhoto.titleLabel?.font = UIFont (name: "Exo2-Regular", size: 19)!
            addPhoto.titleLabel?.textColor = color
            

    }
    @IBAction func backBtn(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true);
    }
    

}
