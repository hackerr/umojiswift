//
//  MusicFromITunesViewController.swift
//  UMOJIswift
//
//  Created by macOS on 9/28/16.
//  Copyright © 2016 DimaMykolykMAC. All rights reserved.
//

import UIKit
import AVFoundation

class MusicFromITunesViewController: UIViewController {
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var contentView : UIView!
    
    
    
    let rangeSlider = RangeSlider(frame: CGRect(x: 6, y: 220, width: 363, height: 31))
    
    //(x : 6, y : 220, width : 363, hight: 31)
    
    private var player : AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSlider()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addSlider (){
        contentView.addSubview(rangeSlider)
        rangeSlider.addTarget(self, action: #selector(MusicFromITunesViewController.rangeSliderValueChanged(_:)), for: .valueChanged)        //https://github.com/warchimede/RangeSlider
    }
    
    func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
    }


    
    @IBAction func playBtn(_ sender: AnyObject) {
        do {
        
            TextToSpeach(text: textView.text, lang: "en-us")
            
        }
        catch let error as NSError {
            self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }

    }
    
    public func TextToSpeach(text : String, lang : String)
    {
        let url : String = "key=1a1b0947d5b04e88a78669208704df1e&hl=\(lang)&src=\(text)&f=48khz_16bit_stereo".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let urll = URL(string: "http://api.voicerss.org/?\(url)")
        
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: urll!, completionHandler: { (URLL, response, error) -> Void in
            do{
                
            let originPath = URLL!
            let destinationPath = URLL!.absoluteString.replacingOccurrences(of: ".tmp", with: ".mp3")
            let fileUrl = URL(string: destinationPath)
            try FileManager.default.moveItem(at: URLL!, to: fileUrl!) //(at: toAtURL(originPath, toURL: fileUrl!)
            
            self.player = AVPlayer(url: fileUrl!)
            self.player?.volume = 1.0
            self.player?.play()
            }
            catch let errorr as NSError {
                    print(errorr)
            }
            
        })
        
        downloadTask.resume()
        
        
        
    }

    @IBAction func backToViewController(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
}
