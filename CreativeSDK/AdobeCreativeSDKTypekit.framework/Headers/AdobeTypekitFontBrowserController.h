/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2015 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

#import <UIKit/UIKit.h>

@protocol AdobeTypekitFontBrowserDelegate;

/**
 * AdobeTypekitFontBrowserController is the class through which user interacts with Font Browser component.  
 * Font Browser displays all font families available to the user.
 * Tapping on a family takes the user to the family details view.
 * Syncing a font from the details view adds it to the user’s synced fonts set and downloads it to the user's device to use it locally.
 */
@interface AdobeTypekitFontBrowserController : UINavigationController

/** @name Properties */
/**
 * Delegate that notifies the presenter of this view controller that the close button has been tapped.
 */
@property (weak, nonatomic) id<AdobeTypekitFontBrowserDelegate> browserDelegate;

/**
 * The tint color of Typekit UI, this can be customized to match the app's color.
 * Default is #2098F5.
 */
@property (strong, nonatomic) UIColor *tintColor;

/**
 * The tint color of the navigation bar, this can be customized to match the app's color.
 * Default is #3F4C56.
 */
@property (strong, nonatomic) UIColor *navigationBarTintColor;

@end

/**
 * Defines a method for responding to user closing Font Browser
 */
@protocol AdobeTypekitFontBrowserDelegate <NSObject>

/** @name Methods */
/**
 * Called when the close button is tapped.
 */
- (void)adobeTypekitFontBrowserUserDidCancel:(AdobeTypekitFontBrowserController *)fontBrowser;

@end
