/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2016 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 *
 * THIS FILE IS PART OF THE CREATIVE SDK PUBLIC API
 *
 ******************************************************************************/

#ifdef ADOBEPUBLISH_WATCHOS2
#import <WatchKit/WatchKit.h>
#else
#import <UIKit/UIKit.h>
#endif

#if !defined(ADOBEPUBLISH_WATCHOS2) && !defined(ADOBEPUBLISH_TVOS)
#import <UIKit/UIGestureRecognizerSubclass.h>
#endif

//! Project version number for AdobeCreativeSDKBehance.
FOUNDATION_EXPORT double AdobeCreativeSDKBehanceVersionNumber;

//! Project version string for AdobeCreativeSDKBehance.
FOUNDATION_EXPORT const unsigned char AdobeCreativeSDKBehanceVersionString[];

#import <AdobeCreativeSDKBehance/AdobePublish.h>
#import <AdobeCreativeSDKBehance/AdobePublishError.h>

#import <AdobeCreativeSDKBehance/AdobePublishAccountType.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityActionType.h>


#if !defined(ADOBEPUBLISH_WATCHOS2) && !defined(ADOBEPUBLISH_TVOS)
#import <AdobeCreativeSDKBehance/AdobePublishDelegate.h>
#import <AdobeCreativeSDKBehance/AdobePublishURLDelegate.h>

#import <AdobeCreativeSDKBehance/AdobePublishComponentDelegate.h>
#import <AdobeCreativeSDKBehance/AdobePublishProfileDelegate.h>
#import <AdobeCreativeSDKBehance/AdobePublishProjectDelegate.h>
#import <AdobeCreativeSDKBehance/AdobePublishProjectViewDelegate.h>
#import <AdobeCreativeSDKBehance/AdobePublishWIPConversion.h>
#import <AdobeCreativeSDKBehance/AdobePublishWIPDelegate.h>
#import <AdobeCreativeSDKBehance/AdobePublishWIPViewDelegate.h>

#endif


#import <AdobeCreativeSDKBehance/AdobePublishServices.h>

#import <AdobeCreativeSDKBehance/AdobePublishBaseModel.h>


#if !defined(ADOBEPUBLISH_WATCHOS2)
#import <AdobeCreativeSDKBehance/AdobePublishActivity.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityAction.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityActionItem.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityAppreciation.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityItem.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityNewProject.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityNewProjectComment.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityNewWIP.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivityProjectFeature.h>
#import <AdobeCreativeSDKBehance/AdobePublishActivitySavedToCollection.h>
#endif


#import <AdobeCreativeSDKBehance/AdobePublishCollection.h>
#import <AdobeCreativeSDKBehance/AdobePublishComment.h>
#import <AdobeCreativeSDKBehance/AdobePublishContentObject.h>
#import <AdobeCreativeSDKBehance/AdobePublishCuratedGallery.h>
#import <AdobeCreativeSDKBehance/AdobePublishError.h>
#import <AdobeCreativeSDKBehance/AdobePublishImageRef.h>
#import <AdobeCreativeSDKBehance/AdobePublishNetworkResponse.h>
#import <AdobeCreativeSDKBehance/AdobePublishProfile.h>
#import <AdobeCreativeSDKBehance/AdobePublishProject.h>
#import <AdobeCreativeSDKBehance/AdobePublishSearchField.h>
#import <AdobeCreativeSDKBehance/AdobePublishSearchFields.h>
#import <AdobeCreativeSDKBehance/AdobePublishTeam.h>
#import <AdobeCreativeSDKBehance/AdobePublishTool.h>
#import <AdobeCreativeSDKBehance/AdobePublishUser.h>
#import <AdobeCreativeSDKBehance/AdobePublishWIPRevision.h>
#import <AdobeCreativeSDKBehance/AdobePublishWorkExperience.h>
#import <AdobeCreativeSDKBehance/AdobePublishWorkInProgress.h>


#if !defined(ADOBEPUBLISH_WATCHOS2) && !defined(ADOBEPUBLISH_TVOS) && !defined(ADOBEPUBLISH_EXTENSION)
#import <AdobeCreativeSDKBehance/AdobePublishCommentPanelButton.h>
#endif

#if !defined(ADOBEPUBLISH_WATCHOS2) && !defined(ADOBEPUBLISH_TVOS)
#import <AdobeCreativeSDKBehance/AdobePublishProfileSpecs.h>
#import <AdobeCreativeSDKBehance/AdobePublishProfileEditor.h>

#import <AdobeCreativeSDKBehance/AdobePublishProjectSpecs.h>
#import <AdobeCreativeSDKBehance/AdobePublishWIPSpecs.h>

#import <AdobeCreativeSDKBehance/AdobePublishPublishing.h>

#import <AdobeCreativeSDKBehance/AdobePublishViewing.h>
#endif
