/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2016 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 *
 **************************************************************************/

#import <UIKit/UIKit.h>

//! Project version number for AdobeCreativeSDKLabs.
FOUNDATION_EXPORT double AdobeCreativeSDKLabsVersionNumber;

//! Project version string for AdobeCreativeSDKLabs.
FOUNDATION_EXPORT const unsigned char AdobeCreativeSDKLabsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AdobeCreativeSDKLabs/PublicHeader.h>
#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>

#import <AdobeCreativeSDKLabs/AdobeLabsMagicMusicRetargeting.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicStyle.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicBrush.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicAudioSpeechMatcher.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicCropper.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicCurve.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicCutoutMaker.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicDepthMapper.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicPath.h>
#import <AdobeCreativeSDKLabs/AdobeLabsMagicVectorizer.h>
#import <AdobeCreativeSDKLabs/AdobeLabsUXMagicPerspectiveView.h>
#import <AdobeCreativeSDKLabs/AdobeLabsUXMagicSelectionView.h>
